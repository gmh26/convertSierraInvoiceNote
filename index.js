/*eslint-env node*/

'use strict';

const fs = require('fs');
const os = require('os');
const path = require('path');

const noteStringStartIndex = 33;
const noteStringLength = 30;

const inputFileSpecifier = process.argv[2] || 'test.original.INV';
const inputFilePath = path.join(__dirname, inputFileSpecifier);
const inputString = fs.readFileSync(inputFilePath, { encoding:'utf8' });
const inputLines = inputString.replace(/\r\n/g, "\r").replace(/\n/g, "\r").split(/\r/);

const outputLines = inputLines.map((line, index) => {

	if (index === 0) return line;
	if (line === '') return line;


	const inputNoteString = line.substr(noteStringStartIndex, noteStringLength);
	const inputNoteMatches = inputNoteString.match(/^([0-9]+)?[(].{5}[)]-([0-9]+)?[(].{5}[)]/);
	if (!inputNoteMatches) {
		console.log(`LINE ${index + 1}: returning original line, the inputNoteString did not match the regular expression`); // eslint-disable-line no-console
		return line;
	}

	const versionNumberStrings = inputNoteMatches.slice(1, 3);
	const versionNumbers = versionNumberStrings.map((string) => {
		if (string === undefined) return undefined;
		const number = parseInt(string, 10);
		if (!Number.isInteger(number)) throw new Error('defined versionNumberString is not an integer');
		return number;
	});

	let outputNoteString = 'EBSCO Renewal 2019 ';
	if (versionNumbers[0] !== undefined) outputNoteString += 'v.' + versionNumbers[0];
	if (versionNumbers[1] !== undefined && versionNumbers[0] !== versionNumbers[1]) outputNoteString += '-' + versionNumbers[1];

	return (
		line.slice(0, noteStringStartIndex)
		+ outputNoteString.padEnd(noteStringLength)
		+ line.slice(noteStringStartIndex + noteStringLength)
	);

});

const outputString = outputLines.join(os.EOL);
const outputFileSpecifier = process.argv[3] || 'output.INV';
const outputFilePath = path.join(__dirname, outputFileSpecifier);
fs.writeFileSync(outputFilePath, outputString, { encoding:'utf8' });

console.log('SCRIPT DONE. NO ERRORS.') // eslint-disable-line no-console